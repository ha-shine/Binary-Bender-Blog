package com.binarybender.web;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.binarybender.domain.AboutMe;
import com.binarybender.domain.AboutMeDao;
import com.binarybender.domain.BlogPost;
import com.binarybender.domain.BlogPostDao;
import com.binarybender.domain.PhotoObject;
import com.binarybender.domain.PhotoObjectDao;

@Controller
@RequestMapping("/administration")
public class AdministrationController {
  
  @Autowired
  private ApplicationContext appContext;
  
  @RequestMapping(method = RequestMethod.GET)
  public String getAdministration(Model model) {
    BlogPostDao blogPostDao = appContext.getBean(BlogPostDao.class);
    model.addAttribute("blogposts", blogPostDao.getAll());
    return "administration/administration-blogposts";
  }
  
  @RequestMapping(path="create", method = RequestMethod.GET)
  public String getBlogCreationForm(Model model) {
    BlogPost blogPost = new BlogPost();
    blogPost.setPublished(true);
    blogPost.setCreatedDate(new Timestamp(System.currentTimeMillis()));
    blogPost.setTitle("Title goes here");
    blogPost.setContent("Edit your content here. Lorem ipsum etc...");
    model.addAttribute("blogpost", blogPost);
    model.addAttribute("formAction", "create");
    return "administration/administration-createblog";
  }
  
  @RequestMapping(path="create", method = RequestMethod.POST) 
  public String postBlog(@ModelAttribute("blogpost") BlogPost blogpost) {
    blogpost.setCreatedDate(new Timestamp(System.currentTimeMillis()));
    blogpost.setLastModifiedDate(new Timestamp(System.currentTimeMillis()));
    BlogPostDao blogPostDao = appContext.getBean(BlogPostDao.class);
    blogPostDao.createBlogPost(blogpost);
    
    return "redirect:/administration";
  }
  
  @RequestMapping(path="edit/{id}", method = RequestMethod.GET)
  public String getEditBlogPost(@PathVariable int id, Model model) {
    BlogPostDao blogPostDao = appContext.getBean(BlogPostDao.class);
    BlogPost blogpost = blogPostDao.searchById(id);
    model.addAttribute("blogpost", blogpost);
    model.addAttribute("formAction", "edit/" + blogpost.getId());
    return "administration/administration-createblog";
  }
  
  @RequestMapping(path="edit/{id}", method = RequestMethod.POST)
  public String editBlogPost(@ModelAttribute("blogpost") BlogPost blogpost, @PathVariable long id) {
    BlogPostDao blogPostDao = appContext.getBean(BlogPostDao.class);
    blogpost.setId(id);
    blogpost.setLastModifiedDate(new Timestamp(System.currentTimeMillis()));
    blogPostDao.modifyBlogPost(blogpost);
    return "redirect:/administration";
  }
  
  @RequestMapping(path="delete/{id}", method = RequestMethod.GET)
  public String deleteBlogPost(@PathVariable int id) {
    BlogPostDao blogPostDao = appContext.getBean(BlogPostDao.class);
    blogPostDao.deleteBlogPost(id);
    
    return "redirect:/administration";
  }
  
  @RequestMapping(path="publish/{id}", method = RequestMethod.GET)
  public String togglePublish(@PathVariable int id) {
    BlogPostDao blogPostDao = appContext.getBean(BlogPostDao.class);
    BlogPost blogPost = blogPostDao.searchById(id);
    
    blogPost.setPublished(!blogPost.isPublished());
    blogPostDao.modifyBlogPost(blogPost);
    
    return "redirect:/administration";
  }
  
  @RequestMapping(path="stats", method = RequestMethod.GET)
  public String showStats() {
    return "administration/administration-stats";
  }
  
  @RequestMapping(path="aboutme", method = RequestMethod.GET)
  public String redirectHTML() {
    return "redirect:aboutme/html";
  }
  
  @RequestMapping(path="aboutme/html", method = RequestMethod.GET)
  public String getAboutMePage(Model model) {
    AboutMeDao aboutMeDao = appContext.getBean(AboutMeDao.class);
    AboutMe aboutmeHTML = aboutMeDao.findByKey("aboutmeHTML");
    if (aboutmeHTML != null) {
      model.addAttribute("aboutmeHTML", aboutmeHTML.getValue());
    }
    return "administration/administration-aboutme-html";
  }
  
  @RequestMapping(path="aboutme/content", method = RequestMethod.GET)
  public String getAboutMeContent(Model model) {
    AboutMeDao aboutMeDao = appContext.getBean(AboutMeDao.class);
    AboutMe aboutmeContent = aboutMeDao.findByKey("aboutmeContent");
    if (aboutmeContent != null) {
      model.addAttribute("aboutmeContent", aboutmeContent.getValue());
    }
    return "administration/administration-aboutme-content";
  }
  
  @RequestMapping(value={"aboutme/html", "aboutme/content"}, method = RequestMethod.POST)
  public String updateAboutMe(@RequestParam Map<String, String> allRequestParams) {
    AboutMeDao aboutMeDao = appContext.getBean(AboutMeDao.class);
    
    List<AboutMe> aboutMeList = new ArrayList<AboutMe>(); 
    for (Map.Entry<String, String> entry : allRequestParams.entrySet()) {
      if (!entry.getKey().equals("_csrf")) {
        AboutMe aboutMe = new AboutMe();
        aboutMe.setKey(entry.getKey());
        aboutMe.setValue(entry.getValue());
        aboutMeList.add(aboutMe);
      }
    }
    
    aboutMeDao.update(aboutMeList);
    return "redirect:/administration/aboutme";
  }
  
  @RequestMapping(path="photos", method = RequestMethod.GET)
  public String getPhotos(Model model) {
    PhotoObjectDao photoObjectDao = appContext.getBean(PhotoObjectDao.class);
    List<PhotoObject> photoObjectList = photoObjectDao.list();
    model.addAttribute("photoList", photoObjectList);
    return "administration/administration-photos";
  }
  
  @RequestMapping(path="photos/upload", method = RequestMethod.POST)
  public String postPhoto(@RequestParam MultipartFile file) {
    PhotoObjectDao photoObjectDao = appContext.getBean(PhotoObjectDao.class);
    PhotoObject photoObject = new PhotoObject();
    photoObject.setPath(file.getOriginalFilename());
    
    if (!file.isEmpty()) {
      try {
        byte[] bytes = file.getBytes();
        photoObjectDao.uploadPhoto(photoObject, bytes);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    return "redirect:/administration/photos";
  }
  
  @RequestMapping(path="photos/delete/{id}", method = RequestMethod.GET)
  public String deletePhoto(@PathVariable long id) {
    PhotoObjectDao photoObjectDao = appContext.getBean(PhotoObjectDao.class);
    photoObjectDao.deletePhotoById(id);
    return "redirect:/administration/photos";
  }
}
