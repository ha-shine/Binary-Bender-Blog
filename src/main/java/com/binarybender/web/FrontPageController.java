package com.binarybender.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.binarybender.domain.AboutMe;
import com.binarybender.domain.AboutMeDao;
import com.binarybender.domain.BlogPost;
import com.binarybender.domain.BlogPostDao;

@Controller
@RequestMapping("/")
public class FrontPageController {
  private int MAX_BLOG_PER_PAGE = 5;
  
  @Autowired
  private ApplicationContext appContext;

  @RequestMapping(method = RequestMethod.GET)
  public String showMainPage(Model model) {
    BlogPostDao blogPostDao = appContext.getBean(BlogPostDao.class);
    long totalPostCount = blogPostDao.getPublishedCount();
    
    model.addAttribute("blogposts", blogPostDao.getPage(1));
    model.addAttribute("haveNewerPosts", false);
    model.addAttribute("haveOlderPosts", haveOlderPosts(totalPostCount, 1));
    model.addAttribute("olderPage", 2);
    
    return "frontpage/frontpage-blogposts";
  }
  
  @RequestMapping(path = "/blog/{page}", method = RequestMethod.GET)
  public String getPageWithPageNumber(@PathVariable int page, Model model) {
    BlogPostDao blogPostDao = appContext.getBean(BlogPostDao.class);
    long totalPostCount = blogPostDao.getPublishedCount();
    
    if (page == 1)
      return "redirect:/";
    
    if (haveOlderPosts(totalPostCount, page - 1))
      return "redirect:/error";
    
    model.addAttribute("blogposts", blogPostDao.getPage(page));
    model.addAttribute("haveNewerPosts", true);
    model.addAttribute("haveOlderPosts", haveOlderPosts(totalPostCount, page));
    model.addAttribute("olderPage", page + 1);
    model.addAttribute("newerPage", page - 1);
    return "frontpage/frontpage-blogposts";
  }
  
  @RequestMapping(path="/aboutme", method = RequestMethod.GET)
  public String showAboutMe(Model model) {
    AboutMeDao aboutMeDao = appContext.getBean(AboutMeDao.class);
    for (AboutMe aboutMe : aboutMeDao.list()) {
      model.addAttribute(aboutMe.getKey(), aboutMe.getValue());
    }
	  return "frontpage/frontpage-aboutme";
  }
  
  @RequestMapping(path="/blogpost/{id}", method = RequestMethod.GET)
  public String getPost(@PathVariable int id, Model model) {
    BlogPostDao blogPostDao = appContext.getBean(BlogPostDao.class);
    BlogPost blogPost = blogPostDao.searchById(id);
    if (blogPost == null)
      return "redirect:/error";
    model.addAttribute("blogpost", blogPostDao.searchById(id));
    
    return "frontpage/frontpage-blogposts";
  }
  
  @RequestMapping(path="/admin-login", method = RequestMethod.GET)
  public String loginFrom() {
    return "frontpage/frontpage-login";
  }
  
  private boolean haveOlderPosts(long totalPostCount, int currentPage) {
    return (Math.ceil(totalPostCount / (double)MAX_BLOG_PER_PAGE) < currentPage + 1);
  }
}
