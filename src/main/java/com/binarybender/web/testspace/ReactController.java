package com.binarybender.web.testspace;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/react")
public class ReactController {

	@RequestMapping(path = "/hello", method = RequestMethod.GET)
	public String helloWorld() {
		return "/testFolder/reacttest";
	}
	
	@RequestMapping(path = "/api/comments", method = RequestMethod.GET)
	public @ResponseBody Comment[] getComments() {
	  Comment[] comments = new Comment[2];
	  comments[0] = new Comment(1, "Pete Hunt", "This is one comment");
	  comments[1] = new Comment(2, "Jordan Walke", "This is another comment");
	  return comments;
	}
}
