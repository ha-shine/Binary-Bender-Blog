package com.binarybender.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.binarybender.config.components.PhotosPathResolver;

@Configuration
public class WebMvcBeans {

  @Bean
  public WebMvcConfigurerAdapter photoPathResolver() {
    return new PhotosPathResolver();
  }
}
