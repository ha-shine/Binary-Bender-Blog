package com.binarybender.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.postgresql.Driver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@ComponentScan("com.binarybender.domain")
public class DataSourceConfiguration {

  @Bean
  public PlatformTransactionManager transactionManager() {
    return new HibernateTransactionManager(sessionFactory().getObject());
  }
  
  @Bean
  public LocalSessionFactoryBean sessionFactory() {
    LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
    sessionFactoryBean.setDataSource(dataSource());
    sessionFactoryBean.setHibernateProperties(hibernateProperties());
    sessionFactoryBean.setPackagesToScan("com.binarybender.domain");
    return sessionFactoryBean;
  }
  
  @Bean
  public DataSource dataSource() {
    BasicDataSource dataSource = new BasicDataSource();
    dataSource.setDriverClassName(Driver.class.getName());
    dataSource.setUrl("jdbc:postgresql:binarybender_blogdb");
    dataSource.setUsername("dummyaccount");
    dataSource.setPassword("dummypassword");
    return dataSource;
  }
  
  private Properties hibernateProperties() {
    Properties properties = new Properties();
    properties.put("hibernate.dialect", org.hibernate.dialect.PostgreSQL9Dialect.class.getName());
    properties.put("hibernate.show_sql", true);
    return properties;
  }
}
