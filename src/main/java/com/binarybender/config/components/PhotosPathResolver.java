package com.binarybender.config.components;

import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

public class PhotosPathResolver extends WebMvcConfigurerAdapter {

  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    String homeDir = System.getProperty("user.home");
    registry.addResourceHandler("/photos/**").addResourceLocations("file:" + homeDir + "/Pictures/ProjectFolder/");
    super.addResourceHandlers(registry);
  }
}
