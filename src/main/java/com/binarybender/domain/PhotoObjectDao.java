package com.binarybender.domain;

import java.util.List;

public interface PhotoObjectDao {

  public void uploadPhoto(PhotoObject photo, byte[] actualPhoto);
  public void deletePhotoById(long id);
  public List<PhotoObject> list();
  public String getPhotoPathById(long id);
}
