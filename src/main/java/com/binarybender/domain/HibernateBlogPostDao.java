package com.binarybender.domain;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("blogPostDao")
public class HibernateBlogPostDao implements BlogPostDao {
  private int MAX_BLOG_PER_PAGE = 5;
  private SessionFactory sessionFactory;
  
  @Autowired
  public void setSessionFactory(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  @Override
  @Transactional
  public void createBlogPost(BlogPost blogPost) {
    sessionFactory.getCurrentSession().save(blogPost);
  }

  @Override
  @Transactional
  public void modifyBlogPost(BlogPost blogPost) {
    sessionFactory.getCurrentSession().update(blogPost);
  }

  @Override
  @Transactional
  public void deleteBlogPost(long id) {
	  BlogPost blogPost = (BlogPost) sessionFactory.getCurrentSession().get(BlogPost.class, id);
	  sessionFactory.getCurrentSession().delete(blogPost);
  }

  @Override
  @Transactional(readOnly = true)
  public BlogPost searchById(long id) {
    return (BlogPost) sessionFactory.getCurrentSession().get(BlogPost.class, id);
  }

  @SuppressWarnings("unchecked")
  @Override
  @Transactional(readOnly = true)
  public List<BlogPost> getAll() {
    Query query = sessionFactory.getCurrentSession().createQuery("FROM BlogPost ORDER BY createdDate DESC");
    return query.list();
  }

  @Override
  @Transactional(readOnly = true)
  public long getTotalCount() {
    Query query = sessionFactory.getCurrentSession().createQuery("SELECT count(*) FROM BlogPost");
    return (long) query.uniqueResult();
  }
  
  @Override
  @Transactional(readOnly = true)
  public long getPublishedCount() {
    Query query = sessionFactory.getCurrentSession().createQuery("SELECT count(*) FROM BlogPost WHERE published=true");
    return (long) query.uniqueResult();
  }

  @SuppressWarnings("unchecked")
  @Override
  @Transactional(readOnly = true)
  public List<BlogPost> getPage(int page) {
    Query query = sessionFactory.getCurrentSession().createQuery("FROM BlogPost WHERE published=true ORDER BY createdDate DESC");
    query.setFirstResult(((page - 1) * MAX_BLOG_PER_PAGE));
    query.setMaxResults(MAX_BLOG_PER_PAGE);
    return query.list();
  }

}
