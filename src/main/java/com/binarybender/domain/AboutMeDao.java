package com.binarybender.domain;

import java.util.List;

public interface AboutMeDao {
  
  public void update(List<AboutMe> list);
  public List<AboutMe> list();
  public AboutMe findByKey(String key);
}
