package com.binarybender.domain;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("aboutMeDao")
public class HibernateAboutMeDao implements AboutMeDao {

  private SessionFactory sessionFactory;
  
  @Autowired
  public void sessionFactory(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  @Override
  @Transactional
  public void update(List<AboutMe> list) {
    for (AboutMe aboutMe : list) {
      sessionFactory.getCurrentSession().saveOrUpdate(aboutMe);
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  @Transactional(readOnly = true)
  public List<AboutMe> list() {
    Query query = sessionFactory.getCurrentSession().createQuery("FROM AboutMe");
    return query.list();
  }

  @Override
  @Transactional(readOnly = true)
  public AboutMe findByKey(String key) {
    return (AboutMe)sessionFactory.getCurrentSession().get(AboutMe.class, key);
  }
  
}
