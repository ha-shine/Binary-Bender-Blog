package com.binarybender.domain;

import java.util.List;

public interface BlogPostDao {

  public void createBlogPost(BlogPost blogPost);
  public void modifyBlogPost(BlogPost blogPost);
  public void deleteBlogPost(long id);
  public BlogPost searchById(long id);
  public List<BlogPost> getAll();
  public long getTotalCount();
  public long getPublishedCount();
  public List<BlogPost> getPage(int page);
}
