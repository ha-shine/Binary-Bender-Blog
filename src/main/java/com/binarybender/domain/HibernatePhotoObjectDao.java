package com.binarybender.domain;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("photoObjectDao")
public class HibernatePhotoObjectDao implements PhotoObjectDao {

  private String filepath = System.getProperty("user.home") + "/Pictures/ProjectFolder/" ;
  private SessionFactory sessionFactory;
  
  @Autowired
  public void setSessionFactory(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  @Override
  @Transactional
  public void uploadPhoto(PhotoObject photoObject, byte[] actualPhoto) {
    String[] parts = photoObject.getPath().split("\\.");
    String extension = "." + parts[parts.length - 1];
    
    sessionFactory.getCurrentSession().save(photoObject);
    
    String filename = photoObject.getId() + extension;
    photoObject.setPath(filename);
    
    try {
      BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(filepath + filename)));
      stream.write(actualPhoto);
      stream.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    
    sessionFactory.getCurrentSession().saveOrUpdate(photoObject);
  }

  @Override
  @Transactional
  public void deletePhotoById(long id) {
    PhotoObject photoObject = (PhotoObject) sessionFactory.getCurrentSession().get(PhotoObject.class, id);
    File actualFile = new File(filepath + photoObject.getPath());
    
    if (actualFile.delete()) {
      sessionFactory.getCurrentSession().delete(photoObject);
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  @Transactional(readOnly = true)
  public List<PhotoObject> list() {
    Query query = sessionFactory.getCurrentSession().createQuery("FROM PhotoObject");
    return query.list();
  }

  @Override
  @Transactional(readOnly = true)
  public String getPhotoPathById(long id) {
    PhotoObject photoObject = (PhotoObject) sessionFactory.getCurrentSession().get(PhotoObject.class, id);
    return photoObject.getPath();
  }
}
