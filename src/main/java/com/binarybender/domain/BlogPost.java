package com.binarybender.domain;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "BLOGPOST")
public class BlogPost {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private long id;
  
  @Column(name = "TITLE")
  private String title;

  @Column(name = "CREATEDDATE")
  private Timestamp createdDate;
  
  @Column(name = "LASTMODIFIEDDATE")
  private Timestamp lastModifiedDate;
  
  @Column(name = "CONTENT")
  private String content;
  
  @Column(name = "PUBLISHED")
  private boolean published;
  
  public BlogPost() { }

  public BlogPost(long id, String title, String content, Timestamp createdDate, Timestamp lastModifiedDate, boolean published) {
    super();
    this.id = id;
    this.title = title;
    this.content = content;
    this.createdDate = createdDate;
    this.lastModifiedDate = lastModifiedDate;
    this.published = published;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Timestamp createdDate) {
    this.createdDate = createdDate;
  }

  public Date getLastModifiedDate() {
    return lastModifiedDate;
  }

  public void setLastModifiedDate(Timestamp lastModifiedDate) {
    this.lastModifiedDate = lastModifiedDate;
  }
  
  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public boolean isPublished() {
    return published;
  }

  public void setPublished(boolean published) {
    this.published = published;
  }
}
